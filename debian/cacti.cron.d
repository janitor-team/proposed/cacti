MAILTO=root
*/5 * * * * www-data php /usr/share/cacti/site/poller.php 2>&1 >/dev/null | if [ -f /usr/bin/ts ] ; then ts ; else tee ; fi >> /var/log/cacti/poller-error.log

# it is recommended to regularly refresh the csrf key; however, as
# that potentially impacts user experience, it should be done during
# non-production hours. As the cacti package doesn't know when that is
# on this system, this cron entry serves as an example that should be
# adjusted to a proper time and enabled.
#30 2 * * sat www-data /usr/share/cacti/cli/refresh_csrf.php
